<?php

/*Szinte mindenben megegyezik a layerLister.php-val, de itt mmár konkrét fájlneveket keresünk a munkacsoporton belül,
és azzal dolgozunk. Nem kell a megnyitott fájlok ellenőrzése, hiszen azt már kilőttük korábban.*/

if (isset($_POST["group"]) && isset($_POST["files"])) {
    $workGroup = $_POST["group"];
    $files = $_POST["files"];
}

/*$workGroup = "dhte";
$files = 'dhte_terep_AA.gpkg,dhte_terep_FAT.gpkg';*/
$selectedFiles = explode(",", $files);
#var_dump($selectedFiles);

# Get geopackage files from adatok folder.

$personalDirs = array();
$personalDirs = scandir("../../../terep/$workGroup/adatok");
$personalDirs = array_diff($personalDirs, array(".", "..", "master"));
$dataBases = array();

foreach ($personalDirs as $personalDir) {
    $contents = glob("../../../terep/$workGroup/adatok/$personalDir/*.gpkg*");
    foreach ($contents as $content) {
        if (!is_dir($content)) {
            $dataBases[$personalDir] = ($content);
        }
    }
}

#var_dump($dataBases);

foreach ($dataBases as $dataDir => $dataBase) {
    $dataBases[$dataDir] = preg_replace('/(.*\/terep\/' . $workGroup . '\/adatok\/' . $dataDir . '\/)([A-Za-z0-9]*_terep_[A-Z]*\.gpkg.{0,4})/', '$2', $dataBase);
}

#var_dump($dataBases);

foreach ($dataBases as $dataDir => $dataBase) {
    $dataBases[$dataDir] = preg_replace('/([A-Za-z0-9}*_terp[A-Z]*\.gpkg)(.{0,4})/', '$1', $dataBase);
}

#var_dump($dataBases);

$dataBases = array_unique($dataBases);

#var_dump($dataBases);

$dataBases = array_intersect($dataBases, $selectedFiles);

#var_dump($dataBases);

foreach ($dataBases as $dataDir => $dataBase) {
    $ogrOutput = shell_exec(
        "ogrinfo ../../../terep/$workGroup/adatok/$dataDir/$dataBase -sql \"SELECT table_name geometry_type_name FROM gpkg_geometry_columns WHERE geometry_type_name = 'POINT'\""
    );
    #var_dump ($ogrOutput);
    $layers[$dataDir] = preg_grep('/(geometry_type_name\s\(String\)\s=)(.*)/', explode("\n", $ogrOutput));
}

foreach ($layers as $layerArray) {
    $layers[] = array_values($layerArray);
}

#var_dump($layers);
$layerList = [];
foreach ($layers as $layerArray) {
    foreach ($layerArray as $layer) {
        array_push($layerList, $layer);
    }
}

foreach ($layerList as $key => $layer) {
    $layerList[$key] = preg_replace('/(.*\s=\s)(.*)/', '$2', $layer);
}

$layerList = array_unique($layerList);
$layerList = array_combine($layerList, $layerList);

#var_dump($layerList);

echo json_encode($layerList, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT);
