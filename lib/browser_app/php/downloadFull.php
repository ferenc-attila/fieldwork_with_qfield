<?php


if (isset($_POST["group"])) {

    $workGroup = $_POST ["group"];
	
	//checking if there's temp folder 
	chdir ("../../../etc");
	$folders = glob("temp");
	if (empty($folders)){
		mkdir("temp");
	}
	chdir ("../");
	
	//Create archive
	$downloadTime = date("Y_m_d_H_i_s");
	$rootPath = realpath ("terep/$workGroup");
	
	
	fopen("etc/temp/".$workGroup."_bckp_".$downloadTime.".zip", "w");
	$zip=new ZipArchive;
	$zipfile="etc/temp/".$workGroup."_bckp_".$downloadTime.".zip";
	$res = $zip->open($zipfile, ZipArchive::CREATE | ZipArchive::OVERWRITE);
	if ($res === TRUE) {
		$files = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator($rootPath),
			RecursiveIteratorIterator::LEAVES_ONLY
		);
		
		foreach ($files as $name => $file)
		{
			// Skip directories (they would be added automatically)
			if (!$file->isDir())
			{
				// Get real and relative path for current file
				$filePath = $file->getRealPath();
				$relativePath = substr($filePath, strlen($rootPath) + 1);
		
				// Add current file to archive
				$zip->addFile($filePath, $relativePath);
			}
		}
		
		// Zip archive will be created only after closing object
		$zip->close();	
	}
	
	
} else {
	$workGroup = null;
    error_log("No group selected!");
}

//Send file route to JS
echo $zipfile;

?>