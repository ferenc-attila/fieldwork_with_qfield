<?php
if (isset($_POST["scope"]) && isset($_POST["url"])) {
   $scope = $_POST["scope"];
   $url = $_POST["url"];
}

/*
$scope = "get_form_data";
$url = "http://obm.bnpi.hu/projects/dhte/";
$value = 201;
*/

$url = $url."pds.php";

#var_dump($url);

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$headers = array(
   "Content-Type: application/x-www-form-urlencoded",
);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

$data = "access_token=123456789&scope=$scope";

if (isset($_POST["formId"]) && $scope == "get_form_data") {
   $formId = $_POST["formId"];
   $data = $data."&value=".$formId;
}

/*
$formId = 201;
$data = $data."&value=".$formId;
*/

curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

//for debug only!
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$resp = curl_exec($curl);
curl_close($curl);

$resp = json_decode($resp, true);

#var_dump($resp);

if ($scope == "get_project_list") {
   
   $projectDescriptions = array();
   
   foreach ($resp["data"] as $project) {
      $description = "project_description";
      $url = "project_url";
      $projectDescription = $project[$description];
      $projectUrl = $project[$url];
      $projectDescriptions[$projectUrl] = $projectDescription;
   }
   
   echo json_encode($projectDescriptions, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT);

} elseif ($scope == "get_form_list") {
   
   $formNames = array();
   
   foreach ($resp["data"] as $form) {
      $name = "form_name";
      $id = "form_id";
      $formName = $form[$name];
      $formId = $form[$id];
      $formNames[$formId] = $formName;
   }

   echo json_encode($formNames, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT);

} elseif ($scope == "get_form_data") {
   
   echo (json_encode($resp["data"]));
   
}
