<?php

if (isset($_POST["workGroup"]) && isset($_POST["files"]) && isset($_POST["layer"])) {
    $workGroup = $_POST["workGroup"];
    $files = $_POST["files"];
    $layer = $_POST["layer"];
}

/*
$workGroup = 'dhte';
$files = 'dhte_terep_AA.gpkg,dhte_terep_FAT.gpkg';
$layer = 'megfigyelesek';
*/

$selectedFiles = explode(",", $files); //Megegyezik a layerLister tartalmával az ogr parancsig, refaktorálás!

$personalDirs = array();
$personalDirs = scandir("../../../terep/$workGroup/adatok");
$personalDirs = array_diff($personalDirs, array(".", "..", "master"));
$dataBases = array();

foreach ($personalDirs as $personalDir) {
    $contents = glob("../../../terep/$workGroup/adatok/$personalDir/*.gpkg*");
    foreach ($contents as $content) {
        if (!is_dir($content)) {
            $dataBases[$personalDir] = ($content);
        }
    }
}

#var_dump($dataBases);

foreach ($dataBases as $dataDir => $dataBase) {
    $dataBases[$dataDir] = preg_replace('/(.*\/terep\/' . $workGroup . '\/adatok\/' . $dataDir . '\/)([A-Za-z0-9]*_terep_[A-Z]*\.gpkg.{0,4})/', '$2', $dataBase);
}

#var_dump($dataBases);

foreach ($dataBases as $dataDir => $dataBase) {
    $dataBases[$dataDir] = preg_replace('/([A-Za-z0-9}*_terep_[A-Z]*\.gpkg)(.{0,4})/', '$1', $dataBase);
}

#var_dump($dataBases);

$dataBases = array_unique($dataBases);

#var_dump($dataBases);

$dataBases = array_intersect($dataBases, $selectedFiles);

#var_dump($dataBases);

$exporttime=date("Y_m_d_H_i_s");
$dataDirs = array_keys($dataBases);
$dataDirs = implode("_", $dataDirs);

$tempfolder = glob("../../../terep/$workGroup/temp");
if (empty($tempfolder)){
    mkdir("../../../terep/$workGroup/temp");
}
$jsonfolder = glob("../../../terep/$workGroup/temp/json");
if (empty($jsonfolder)){
    mkdir("../../../terep/$workGroup/temp/json");
}

foreach ($dataBases as $dataDir => $dataBase) {
    $ogrOutput = shell_exec(
        "ogr2ogr -append -addfields -fieldTypeToString Date,DateTime,Time -f GeoJSON -s_srs EPSG:23700 -t_srs EPSG:4326 ../../../terep/$workGroup/temp/json/$layer._$dataDirs._$exporttime.json ../../../terep/$workGroup/adatok/$dataDir/$dataBase $layer"
    );
}

//Ha üres a réteg, a böngésző hibát ad ki! Ki kell íratni valamit a felhasználónak!

$json_data = file_get_contents("../../../terep/$workGroup/temp/json/$layer._$dataDirs._$exporttime.json");

//error_log($json_data);
echo (json_encode($json_data, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT));
