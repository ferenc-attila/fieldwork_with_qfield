<?php

function createArchive($workGroup, $filetype, $filenames)
{

    $downloadTime = date("Y_m_d_H_i_s");

    fopen("../../../etc/temp/" . $workGroup . "_" . $filetype . "_bckp_" . $downloadTime . ".zip", "w");
    $zip = new ZipArchive;
    $zipfile = "../../../etc/temp/" . $workGroup . "_" . $filetype . "_bckp_" . $downloadTime . ".zip";
    $res = $zip->open($zipfile, ZipArchive::CREATE | ZipArchive::OVERWRITE);
    if ($res === TRUE) {
        $downloadFiles = array();
        
        if ($filetype == "gpkg") {
            $dataDirs = array();
            $dataDirs = scandir("../../../terep/$workGroup/adatok");
            $dataDirs = array_diff($dataDirs, array(".", "..", "master"));
            $dataBases = array();

            foreach ($dataDirs as $dataDir) {
                $contents = glob("../../../terep/$workGroup/adatok/$dataDir/*");
                foreach ($contents as $content) {
                    if (!is_dir($content)) {
                        $dataBases[] = ($content);
                    }
                }
            }

            foreach ($dataBases as $dataBase) {
                foreach ($filenames as $filename) {
                    if (strpos($dataBase, $filename)) {
                        $downloadFiles[] = $dataBase;
                        $zip->addFile($dataBase,
                        (preg_replace('/(.*)('. $workGroup . '_terep_[A-Z]*\.gpkg.{0,4})/', '$2', $dataBase)));
                    }
                }
            }
        } else if ($filetype == 'qgs') {
            $projectFiles = array();

            $contents = glob("../../../terep/$workGroup/project/*");
            foreach ($contents as $content) {
                if (!is_dir($content)) {
                    $projectFiles[] = ($content);
                }
            }

            foreach ($projectFiles as $projectFile) {
                foreach ($filenames as $filename) {
                    if (strpos($projectFile, $filename)) {
                        $downloadFiles[] = $projectFile;
                        $zip->addFile($projectFile,
                        (preg_replace('/(.*)('. $workGroup . '_terep_[A-Z]*\.qg[s|z])/', '$2', $projectFile)));
                    }
                }
            }
        } else {
            error_log("Not a valid filetype");
        }

        // Zip archive will be created only after closing object
        $zip->close();
    }
    $zipfile = str_replace("../", "", $zipfile);
    echo $zipfile;
}

if (isset($_POST["group"]) && isset($_POST["filenames"]) && isset($_POST["filetype"])) {
    $workGroup = $_POST["group"];
    $filetype = $_POST["filetype"];
    $filenames = $_POST["filenames"];

    $filenames = explode(",", $filenames);

    //checking if there's temp folder 

    $folders = glob("../../../etc/temp");
    if (empty($folders)) {
        mkdir("temp");
    }

    createArchive($workGroup, $filetype, $filenames);

} else {
    error_log("No group or filename selected!");
}
