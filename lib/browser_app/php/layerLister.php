<?php

if (isset($_POST["group"])) {
    $workGroup = $_POST["group"];
}

#$workGroup = "dhte";

# Get geopackage files from adatok folder.

$openedDataBases = array();
$personalDirs = array();
$personalDirs = scandir("../../../terep/$workGroup/adatok");
$personalDirs = array_diff($personalDirs, array(".", "..", "master"));
$dataBases = array();

foreach ($personalDirs as $personalDir) {
    $contents = glob("../../../terep/$workGroup/adatok/$personalDir/*.gpkg*");
    foreach ($contents as $content) {
        if (!is_dir($content)) {
            $dataBases[$personalDir] = ($content);
        }
    }
}

#var_dump($dataBases);

foreach ($dataBases as $dataDir => $dataBase) {
    $dataBases [$dataDir] = preg_replace('/(.*\/terep\/'.$workGroup.'\/adatok\/'.$dataDir.'\/)([A-Za-z0-9]*_terep_[A-Z]*\.gpkg.{0,4})/', '$2', $dataBase);
}

#var_dump($dataBases);

foreach ($dataBases as $dataBase) {
    if (preg_grep('/(.*)(-wal)/', explode("\n", $dataBase)) != NULL){
        $openedDataBases[] = ($dataBase);
    }
}

#var_dump($dataBases);
#var_dump($openedDataBases);

if (empty($openedDataBases)) {
    foreach ($openedDataBases as $key => $fileName) {
        $openedDataBases[$key] = preg_replace('/(.*)(-wal)/', '$1', $fileName);
    }
}

#var_dump($openedDataBases);
#var_dump($dataBases);

foreach ($dataBases as $dataDir => $dataBase) {
    $dataBases[$dataDir] = preg_replace('/([A-Za-z0-9}*_terp[A-Z]*\.gpkg)(.{0,4})/', '$1', $dataBase); //a regexpet átnézni!
}

#var_dump($dataBases);

$dataBases = array_unique($dataBases);

#var_dump($dataBases);

$dataBases = array_diff($dataBases, $openedDataBases);

#var_dump($dataBases);

foreach ($dataBases as $dataDir => $dataBase){
    $ogrOutput = shell_exec (
    "ogrinfo ../../../terep/$workGroup/adatok/$dataDir/$dataBase -sql \"SELECT table_name geometry_type_name FROM gpkg_geometry_columns WHERE geometry_type_name = 'POINT'\"");
    #var_dump ($ogrOutput);
    $layers[$dataDir] = preg_grep('/(geometry_type_name\s\(String\)\s=)(.*)/', explode("\n", $ogrOutput));
}

foreach ($layers as $layerArray) {
    $layers[] = array_values($layerArray);
}

#var_dump($layers);
$layerList = [];
foreach ($layers as $layerArray) {
    foreach ($layerArray as $layer){
        array_push($layerList, $layer);
    }
}

foreach ($layerList as $key => $layer) {
    $layerList[$key] = preg_replace('/(.*\s=\s)(.*)/', '$2', $layer);
    }

#var_dump ($layerList);

$layerList = array_unique($layerList);

echo json_encode ($layerList, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT);