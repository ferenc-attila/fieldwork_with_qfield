<?php

if (isset($_POST["group"]) && isset($_POST["files"]) && isset($_POST["layer"])) {
    $workGroup = $_POST["group"];
    $files = $_POST["files"];
    $layer = $_POST["layer"];
}

/*$workGroup = "dhte";
$files = 'dhte_terep_AA.gpkg,dhte_terep_FAT.gpkg';
$layer = 'megfigyelesek'; */

$dataFolder = "../../../terep/$workGroup/adatok";
$files = explode(",", $files);

$selectedFiles = array();
foreach ($files as $file) {
    $initial = preg_replace('/([A-Za-z0-9]*_terep_)([A-Z]*)(\.gpkg)/', '$2', $file);
    $selectedFiles[$initial] = $file;
}

#var_dump($selectedFiles);

$exportfolder = glob ("../../../terep/$workGroup/export");
if (empty($exportfolder)){
	mkdir("../../../terep/$workGroup/export");
}

$exportfolder = glob ("../../../terep/$workGroup/export/userexport");
if (empty($exportfolder)){
	mkdir("../../../terep/$workGroup/export/userexport");
}

$exporttime=date("Y_m_d_H_i_s");
foreach ($selectedFiles as $dataDir => $dataBase){
    $ogrOutput = shell_exec (
        'ogr2ogr -append -addfields -f "GPKG" ../../../terep/'.$workGroup.'/export/userexport/'.$layer.'_'.$exporttime.'.gpkg ../../../terep/'.$workGroup.'/adatok/'.$dataDir.'/'.$dataBase.' '.$layer);
}

echo '../../../terep/'.$workGroup.'/export/userexport/'.$layer.'_'.$exporttime.'.gpkg';
