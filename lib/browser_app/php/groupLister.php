<?php

// Get folders of workgroups

chdir ("../../../");

$groupDirs = array();
$groupDirs = scandir ("terep");
$groupDirs = array_diff($groupDirs, array(".", "..", "master"));
echo json_encode($groupDirs, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT);
