<?php

if (isset($_POST["projecturl"]) && isset($_POST["formId"]) && isset($_POST["obmHeader"]) && isset($_POST["obmDataBody"]))  {
    $projecturl = $_POST["projecturl"];
    $formId = $_POST["formId"];
    $obmHeader = $_POST["obmHeader"];
    $obmDataBody = $_POST["obmDataBody"];
};
/*
$obmData = json_decode($obmDataBody, true, JSON_OBJECT_AS_ARRAY);
error_log($obmHeader);
error_log($obmDataBody);
*/

$obmHeader = explode(",", $obmHeader);
$obmHeader = json_encode($obmHeader);

//$obmData = '[{"datum":"2021/04/27","idopont":"08:19","idojaras":"Borult","felmeres_mod":"Szórványadatok gyűjtése (zoológia)","gyujtesi_mod":"Vizuális megfigyelés","koord_pont":null,"mmm_szamlalas":null,"adatkozlo":"Antal András","faj":"Otis tarda","egyed":1,"szamossag":"pontos egyedszám","ivar":"hím","kor":"adult","elohely":"lucerna","mikrohabitat":null,"koltohely_azonosito":null,"aktivitas":"napi aktivitás közben","statusz":"élő egyed","viselkedes":null,"megjegyzes":null,"hatarozo":null,"obm_geometry":"Point 20.56041114955521 47.58976662109704"},"1":{"datum":"2021/04/27","idopont":"08:22","idojaras":"Borult","felmeres_mod":"Szórványadatok gyűjtése (zoológia)","gyujtesi_mod":"Vizuális megfigyelés","koord_pont":null,"mmm_szamlalas":null,"adatkozlo":"Antal András","faj":"Upupa epops","egyed":2,"szamossag":"pontos egyedszám","ivar":"párban","kor":"adult","elohely":"energiafű","mikrohabitat":null,"koltohely_azonosito":null,"aktivitas":"napi aktivitás közben","statusz":"élő egyed","viselkedes":null,"megjegyzes":null,"hatarozo":null,"obm_geometry":"Point 20.560150925524148 47.59525631726507"},"2":{"datum":"2021/04/27","idopont":"08:32","idojaras":"Borult","felmeres_mod":"Szórványadatok gyűjtése (zoológia)","gyujtesi_mod":"Vizuális megfigyelés","koord_pont":null,"mmm_szamlalas":null,"adatkozlo":"Antal András","faj":"Aquila heliaca","egyed":1,"szamossag":"pontos egyedszám","ivar":"nőstény","kor":"adult","elohely":"erdő","mikrohabitat":null,"koltohely_azonosito":null,"aktivitas":"napi aktivitás közben","statusz":"élő egyed","viselkedes":"Kotlik","megjegyzes":null,"hatarozo":null,"obm_geometry":"Point 20.562566952625577 47.60697524921794"}]';

$url = $projecturl."pds.php";

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$headers = array(
   "Content-Type: application/x-www-form-urlencoded",
   "Authorization: Bearer 123456789",
);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

$data = "scope=put_data&form_id=".$formId."&header=".$obmHeader."&data=".$obmDataBody."&ignore_warning=1";

curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

//for debug only!
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$resp = curl_exec($curl);
curl_close($curl);
echo($resp);
