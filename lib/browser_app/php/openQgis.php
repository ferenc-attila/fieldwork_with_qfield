<?php

if (isset($_POST["workGroup"]) && isset($_POST["files"]) && isset($_POST["layer"])) {
    $workGroup = $_POST["workGroup"];
    $files = $_POST["files"];
    $layer = $_POST["layer"];
}

/*
$workGroup = 'dhte';
$files = 'dhte_terep_AA.gpkg,dhte_terep_FAT.gpkg';
$layer = 'megfigyelesek';
*/
$selectedFiles = explode(",", $files);

//Megegyezik a layerLister tartalmával az ogr parancsig, refaktorálás!

$personalDirs = array();
$personalDirs = scandir("../../../terep/$workGroup/adatok");
$personalDirs = array_diff($personalDirs, array(".", "..", "master"));
$dataBases = array();

foreach ($selectedFiles as $selectedFile) {
    foreach ($personalDirs as $personalDir) {
        $contents = glob("../../../terep/$workGroup/adatok/$personalDir/$selectedFile");
        if (!empty($contents)) {
            foreach ($contents as $content) {
                if (!is_dir($content)) {
                    $dataBases[$personalDir] = ($content);
                }
            }
        }
    }
}

#var_dump($dataBases);

foreach ($dataBases as $personalDir => $dataBase) {
    $dataBases[$personalDir] = $dataBase."\|layername=".$layer;
}

#var_dump($dataBases);

$layers = implode(" ", $dataBases);

#var_dump($layers);

$qgisVersion = shell_exec('qgis --version');
$qgisVersion = preg_replace('/(QGIS )([0-9]\.[0-9]+\.[0-9]+)(.*)(\s)/', '$2', $qgisVersion);

//var_dump($qgisVersion);

if (version_compare($qgisVersion, "3.0.0", ">=")) {
    echo "Good";
    shell_exec("qgis -- $layers");
} else {
    echo "Bad";
}
