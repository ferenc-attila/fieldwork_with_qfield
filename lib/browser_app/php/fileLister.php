<?php

function listFiles($folder, $extension)
{
    $fileList = array();
    $personalDirs = array();
    $openedDataBases = array();

    $personalDirs = scandir($folder);
    $personalDirs = array_diff($personalDirs, array(".", "..", "master"));

    if ($folder == "adatok") {
        foreach ($personalDirs as $personalDir) {
            if (is_dir("$folder/$personalDir")) {
                chdir("$folder/$personalDir");
                $contents = glob("*.$extension*", GLOB_NOESCAPE);
                chdir("../../");
                $fileList = array_merge($fileList, $contents);
            }
        }
        foreach ($fileList as $file) {
            if (preg_grep('/(.*)(-wal)/', explode("\n", $file)) != NULL) {
                $openedDataBases[] = ($file);
            }
        }
        if (empty($openedDataBases)) {
            foreach ($openedDataBases as $fileName) {
                $fileName = preg_replace('/(.*)(-wal)/', '$1', $fileName);
                error_log($fileName);
                $fileList = array_diff ($fileList, array($fileName));
                $fileList = array_diff ($fileList, array("{$fileName}-shm"));
            }
            echo json_encode($fileList, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT);
        } else {
            echo json_encode($fileList, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT);
        }
    } else if ($folder == "project") {
        chdir($folder);
        $contents = glob("*.$extension", GLOB_NOESCAPE);
        chdir("../");
        $fileList = array_merge($fileList, $contents);
        $fileList = preg_grep('/(.*)_(.*)_([A-Z]).(.*)/', $fileList);

        echo json_encode($fileList, JSON_FORCE_OBJECT, JSON_PRETTY_PRINT);
    }
}

if (isset($_POST["group"]) && isset($_POST["filetype"])) {
    $workGroup = $_POST["group"];
    $filetype = $_POST["filetype"];

#$workGroup = "dhte";
#$filetype = "gpkg";

chdir("../../../terep/$workGroup");

if ($filetype == "gpkg") {
    $fileList = listFiles("adatok", $filetype);
} else if ($filetype == "qgs") {
    $fileList = listFiles("project", $filetype);
}
} else {
    echo json_encode("Error! No group and/or filetype selected!");
}
