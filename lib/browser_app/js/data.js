let hash = (window.location.hash);
let regExp = /(\#group\/text=\')(.+)(\'&files\/text=\')(.+)(\')/;
let variables = hash.match(regExp);
const workGroup = variables[2];
const files = variables[4];

//Ezek a funkciók a main.js-ből származnak, némelyik kissé átalakítva, ott talán egy részük nem is kell majd (refaktorálás!)

function createAnyElement(name, attributes) {
    let element = document.createElement(name);
    for (let k in attributes) {
        element.setAttribute(k, attributes[k]);
    }
    return element;
};

function createExportButtons(select) {
    layer = select.value;
    let buttonDiv = document.getElementById("layerExportButtonDiv");
    buttonDiv.innerHTML = "";
    let dataButtonGroup = createAnyElement("div", {
        type: "div",
        class: "btn-group",
        role: "group"
    });
    let dataDownloadButton = createAnyElement("button", {
        type: "button",
        class: "btn btn-success selectBtn",
        name: "dataDownloadBtn",
        onClick: 'downloadLayer(workGroup, files, layer)'
    });
    dataDownloadButton.innerHTML = "Adatok letöltése";
    if (layer == "megfigyelesek" || layer == "botanika") {
        let obmConnectButton = createAnyElement("button", {
            type: "button",
            class: "btn btn-success selectBtn",
            name: "obmConnectBtn",
            onClick: 'obmProjectList()'
        });
        obmConnectButton.innerHTML = "OBM kapcsolódás";
        buttonDiv.appendChild(dataButtonGroup);
        dataButtonGroup.appendChild(dataDownloadButton);
        dataButtonGroup.appendChild(obmConnectButton);
    } else {
        buttonDiv.appendChild(dataButtonGroup);
        dataButtonGroup.appendChild(dataDownloadButton);
    }
};

function createSelectList(listID, listObject, onChangeFunction, listTitle) {
    let form = document.getElementById(listID + "Form");
    let mainDiv = document.getElementById("mainDiv")
    if (!form) {
        title = createAnyElement("h4");
        title.innerHTML = listTitle;
        form = createAnyElement("form", {
            id: listID + "Form",
            class: "forms"
        });
        form.appendChild(title);
        mainDiv.appendChild(form);
    }
    else {
        form.innerHTML = "";
        if (form.id == "obmProjectForm" || form.id == "obmFormsForm") {
            title = createAnyElement("h4");
            title.innerHTML = listTitle;
            form.appendChild(title);
        }

    }
    let selectDiv = createAnyElement("div");
    let select = createAnyElement("select", {
        id: listID + "List",
        onchange: onChangeFunction,
        class: "form-select form-select-sm"
    });
    let defaultValue = createAnyElement("option", {
        selected: "selected"
    });
    defaultValue.innerHTML = "Válassz egy elemet a listából!";
    select.appendChild(defaultValue);
    for (k in listObject) {
        let selectItem = createAnyElement("option", {
            value: `${k}`
        });
        selectItem.innerHTML = String(listObject[k]);
        select.appendChild(selectItem);
        selectDiv.appendChild(select);
        form.appendChild(selectDiv);
    }
    let buttonDiv = createAnyElement("div", {
        class: "buttonDiv",
        id: "layerExportButtonDiv"
    });
    form.appendChild(buttonDiv);
};

function createLayerList() {
    let form = document.getElementById("layerExportForm");
    form.innerHTML = "";
    let filesData = new FormData();
    filesData.set("group", workGroup);
    filesData.set("files", files);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: filesData
    };

    fetch("../php/layerLister2.php", fetchOptions).then(
        resp => resp.json()
    ).then(
        gisLayers => createSelectList('layerExport', gisLayers, "createExportButtons(this);")
    );
};

function download(file) {
    let filename = file.match(/[^/]+$/g);
    element = createAnyElement("a", {
        href: `${file}`,
        download: filename
    });
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
};

function downloadLayer(workGroup, files, layer) {
    let layerData = new FormData();
    layerData.set("group", workGroup);
    layerData.set("files", files);
    layerData.set("layer", layer);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: layerData
    };

    fetch("../php/downloadLayer.php", fetchOptions).then(
        resp => resp.text()
    ).then(downloadLink => download(downloadLink)
    );
};

function obmProjectList() {
    let projectData = new FormData();
    projectData.set("scope", "get_project_list");
    projectData.set("url", "http://obm.bnpi.hu/");
    let fetchOptions = {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        body: projectData
    };
    return fetch("../php/getObmInfo.php", fetchOptions).then(
        resp => resp.json()
    ).then(
        projects => createSelectList("obmProject", projects, "getFormList(this)", "A szerveren elérhető projektek listája")
    );
};

function obmUploadLayer() {
    let layerData = new FormData();
    layerData.set("group", workGroup);
    layerData.set("files", files);
    layerData.set("layer", layer);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: layerData
    };

};

//Talán a formok kiválasztása nem lesz opció, eleve hozzá lehet rendelni a munkacsoport és a kiválasztott réteg alapján.
function getFormList(select) {
    projecturl = select.value;
    let layerData = new FormData();
    layerData.set("url", projecturl);
    layerData.set("scope", "get_form_list");
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: layerData
    };
    return fetch("../php/getObmInfo.php", fetchOptions).then(
        resp => resp.json()
    ).then(
        forms => createSelectList("obmForms", forms, "showFormDetails(this)", "A szerveren elérhető űrlapok listája")
    );
};

function showFormDetails(select) {
    let formId = select.value;
    let formDetails = new FormData();
    formDetails.set("url", projecturl);
    formDetails.set("scope", "get_form_data");
    formDetails.set("formId", formId);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: formDetails
    };
    return fetch("../php/getObmInfo.php", fetchOptions).then(
        resp => resp.json()
    ).then(
        form => formDetailsToTable(form)
    );
};

function formDetailsToTable(form) {
    obmFormId = document.getElementById("obmFormsList").value;
    let mainDiv = document.getElementById("mainDiv");
    let tableRow = document.getElementById("formTableRow");
    if (!tableRow) {
        tableRow = createAnyElement("row", {
            id: "formTableRow"
        });
        mainDiv.appendChild(tableRow);
    } else {
        tableRow.innerHTML = "";
    }
    let tableTitle = createAnyElement("h4");
    tableTitle.innerHTML = "A kiválasztott űrlap struktúrája";
    tableRow.appendChild(tableTitle);
    let formTable = createAnyElement("table", {
        id: "formTable",
        class: "table table-striped table-sm"
    });
    tableRow.appendChild(formTable);
    let tableHead = createAnyElement("thead");
    formTable.appendChild(tableHead);
    let tableBody = createAnyElement("tbody");
    formTable.appendChild(tableBody);
    let columnNames = ["Mezőnév", "Leírás", "Adatbázis oszlop", " Adattípus"];
    let columnValues = ["short_name", "description", "column", "type"];
    for (i in columnNames) {
        let th = createAnyElement("th");
        th.innerHTML = columnNames[i];
        tableHead.appendChild(th);
    }
    var obmColumns = [];
    for (j in form) {
        tr = createAnyElement("tr");
        tableBody.appendChild(tr);
        obmColumns.push(form[j]["column"]);
        for (k in columnValues) {
            let td = createAnyElement("td");
            td.innerHTML = form[j][columnValues[k]];
            tr.appendChild(td);
        }
    }
    obmColumns = obmColumns.toString();
    let uploadButtonDiv = document.getElementById("uploadButtonDiv");
    if (!uploadButtonDiv) {
        uploadButtonDiv = createAnyElement("div", {
            id: "uploadButtonDiv",
            type: "div",
            class: "btn-group",
            role: "group"
        });
        mainDiv.appendChild(uploadButtonDiv);
    } else {
        uploadButtonDiv.innerHTML = "";
    }
    let restartButton = createAnyElement ("button", {
        type: "button",
        class: "btn btn-warning selectBtn",
        name: "restartBtn",
        onClick: 'window.location.reload(true)'
    });
    restartButton.innerHTML = "Újrakezdés"
    uploadButtonDiv.appendChild(restartButton);

    let uploadButton = createAnyElement ("button", {
        type: "button",
        class: "btn btn-success selectBtn",
        name: "uploadBtn",
    });
    uploadButton.addEventListener("click", function () {
        window.location = `upload.html#group/text='${workGroup}'&files/text='${files}'&layer/text='${layer}'&projecturl/text='${projecturl}'&formId/text='${obmFormId}'&columns/text='${obmColumns}'`
    });
    uploadButton.innerHTML = "Adatellenőrzés"
    uploadButtonDiv.appendChild(uploadButton);
};

createLayerList();