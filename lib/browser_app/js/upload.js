let hash = (window.location.hash);
let regExp = /(\#group\/text=\')(.+)(\'&files\/text=\')(.+)(\'&layer\/text=\')(.+)(\'&projecturl\/text=\')(.+)(\'&formId\/text=\')(.+)(\'&columns\/text=\')(.+)(\')/;
let variables = hash.match(regExp);
const workGroup = variables[2];
const files = variables[4];
const layer = variables[6];
const projecturl = variables[8];
const formId = variables[10];
const obmHeader = variables[12].split(",");

function DeleteKeys(object, array) {
    for (let index = 0; index < array.length; index++) {
        delete object[array[index]];
    }
    return object;
  }

function createAnyElement(name, attributes) {
    let element = document.createElement(name);
    for (let k in attributes) {
        element.setAttribute(k, attributes[k]);
    }
    return element;
};

function getLayerTable() {
    let getLayerData = new FormData();
    getLayerData.set("workGroup", workGroup);
    getLayerData.set("files", files);
    getLayerData.set("layer", layer);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: getLayerData
    };
    return fetch("../php/getLayerData.php", fetchOptions).then(
        resp => resp.json()
    ).then(
        layerData => createLayerDataTable(layerData)
    );
};

//Ha üres a réteg, a böngésző hibát ír ki. Ki kell íratni valamit a felhasználónak!

function createLayerDataTable(data) {
    let mainDiv = document.getElementById("mainDiv");
    let layerTable = document.getElementById("layerAttributeTable");
    if (!layerTable) {
        layerTable = createAnyElement("table", {
            id: "layerAttributeTable",
            class: "display table table-secondary table-bordered table-hover table-sm",
            width: "100%"
        });
        mainDiv.appendChild(layerTable);
    } else {
        layerTable.innerHTML = "";
    }
    jsonData = JSON.parse(data);
    showLayerData(jsonData);
    
    let attributes = jsonData["features"];
    
    let tableHeader = Object.keys(attributes[0]["properties"]);
    let headerDifference = tableHeader.filter(x => !obmHeader.includes(x));

    let columnNames = [];
    for (i in obmHeader) {
        let columnName = {};
        columnName["title"] = obmHeader[i];
        columnName["showColumn"] = true;
        columnNames.push(columnName);
    }
/*    let wktGeometryColumn = {
        title: "obm_geometry",
        showColumn: false
    };
    columnNames.push(wktGeometryColumn);*/
    let jsonGeometryColumn = {
        title: "json_geometry",
        showColumn: false
    };
    columnNames.push(jsonGeometryColumn);
    let rows = [];
    var uploadData = {};
    for (a in attributes) {
        
        var property = DeleteKeys(attributes[a]["properties"], headerDifference);
        uploadData[a] = property;

        let coordinates = attributes[a]["geometry"]["coordinates"].join(" ");
        let wktString = attributes[a]["geometry"]["type"] + " " + coordinates;
        let jsonGeometry = JSON.stringify(attributes[a]["geometry"]);
        let row = Object.values(property);
        row.push(wktString);
        uploadData[a]["obm_geometry"] = wktString;
        row.push(jsonGeometry);
        rows.push(row);
    };

    $('#layerAttributeTable').DataTable({
        data: rows,
        columns: columnNames,
        language: {
            url: '//cdn.datatables.net/plug-ins/1.11.3/i18n/hu.json'
        }
    });
    createEndButtons (uploadData);
};

function onEachFeature(feature, layer) {
    var attributes = Object.keys(feature.properties);
    var popupContent = "";
    for (i in attributes) {
        if (feature.properties[attributes[i]]) {
            popupContent += '<b>' + attributes[i] + '</b>: ' + feature.properties[attributes[i]] + '<br>';
            layer.bindPopup(popupContent);
        }
    };
};

function showLayerData(layer) {
    var geojsonMarkerOptions = {
        radius: 4,
        fillColor: "#999966",
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };


    jsonLayer = L.geoJSON(layer, {

        onEachFeature: onEachFeature,

        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, geojsonMarkerOptions)
        }
    }).addTo(mymap);
    mymap.fitBounds(jsonLayer.getBounds());
}

function createEndButtons (data) {
    let endButtonDiv = createAnyElement ("div", {
        id: "endButtonDiv",
        class: "btn-group",
        role: "group"
    });
    mainDiv.appendChild(endButtonDiv);
/*    let qgisButton = createAnyElement ("button", {
        id: "qgisButton",
        class: "btn btn-success selectBtn",
        onClick: "openInQgis()"
    });
    qgisButton.innerHTML = "Szerkesztés QGIS-ben";
    endButtonDiv.appendChild(qgisButton);*/
    let uploadButton = createAnyElement ("button", {
        id: "uploadButton",
        class: "btn btn-success selectBtn"
    });
    uploadButton.innerHTML = "OBM adatfeltöltés";
    uploadButton.addEventListener("click", function () {
        uploadToObm(data)
    });
    endButtonDiv.appendChild(uploadButton);
}
/*
function openInQgis () {
    let getLayerData = new FormData();
    getLayerData.set("workGroup", workGroup);
    getLayerData.set("files", files);
    getLayerData.set("layer", layer);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: getLayerData
    };
    return fetch("../php/openQgis.php", fetchOptions).then(
        resp => resp.json()
    );
};
*/
function uploadToObm (data) {
    let obmDataBody = JSON.stringify(data);



    curlHeader = obmHeader.toString();
    curlHeader = curlHeader.replace("'", "\"");
    let curlPutData = new FormData;
    curlPutData.set("projecturl", projecturl);
    curlPutData.set("formId", formId);
    curlPutData.set("obmHeader", curlHeader);
    curlPutData.set("obmDataBody", obmDataBody);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: curlPutData
    };
    return fetch("../php/uploadToObm.php", fetchOptions).then(
        resp => resp.json()).then(
            info => console.log(info)
        )
};

/*
We have to recreate a geoJSON based on visible data to visualize on mymap.

function getHtmlTableData() {
    let visibleTable = document.getElementById("layerAttributeTable");
    let visibleAttributes = {};
    let visibleColumnNames = [];
    for (i = 0; i < visibleTable.rows[0].cells.length; i++) {
        visibleColumn = visibleTable.rows[0].cells[i].innerHTML;
        visibleColumnNames.push(visibleColumn);
    };
    for (j = 1; j <visibleTable.rows.length-1; i++)
        for (i = 0; i < visibleColumnNames.length-1; i++) {
            visibleAttribute = {};
            visibleAttribute[visibleColumnNames[i]] = visibleTable.rows[j].cells[i].innerHTML;
            visibleAttributes[j-1] = visibleAttribute;
        }
    console.log(visibleAttributes);
}
*/

getLayerTable();

var mymap = L.map('mapDiv').setView([47.8878, 20.4620], 9);

L.tileLayer('https://osm.dhtecloud.mooo.com/tile/{z}/{x}/{y}.png',
    {
        maxZoom: 18,
        attribution: 'Map data &copy;<a href="https://www.openstreetmap.org/copyright">OpenStreetMap;</a><a href="https://osm.dhtecloud.mooo.com"> BNPI OSM server</a>',
    }
).addTo(mymap);

