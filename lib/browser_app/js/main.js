const listParams = {
    "dataBackup": "gpkg",
    "projectBackup": "qgs",
    "projectUpdate": "qgs"
};

function createAnyElement(name, attributes) {
    let element = document.createElement(name);
    for (let k in attributes) {
        element.setAttribute(k, attributes[k]);
    }
    return element;
};

function getPHPJSON(url) {
    let fetchOptions = {
        method: "GET",
        mode: "cors",
        cache: "no-cache",
    };
    return fetch(`${url}`, fetchOptions).then(
        resp => resp.json(),
        error => console.error(error)
    );
};

function createSelectList(listID, listObject) {
    let form = document.querySelector("#" + listID + "Form");
    form.innerHTML = "";
    let selectDiv = createAnyElement("div");
    let select = createAnyElement("select", {
        id: listID + "List",
        onchange: "groupCap(this);",
        class: "form-select form-select-sm"
    });
    let defaultValue = createAnyElement("option", {
        selected: "selected"
    });
    defaultValue.innerHTML = "Válassz egy elemet a listából!";
    select.appendChild(defaultValue);
    for (k in listObject) {
        let selectItem = createAnyElement("option", {
            value: listObject[k]
        });
        if (listID = 'workGroup') {
            selectItem.innerHTML = String(listObject[k]).toUpperCase();
        } else {
            selectItem.innerHTML = String(listObject[k]);
        }
        select.appendChild(selectItem);
        selectDiv.appendChild(select);
        form.appendChild(selectDiv);
    }
    if (select == document.getElementById("workGroupList")) {
        select.addEventListener("change", createAllCheckBoxes);
        select.addEventListener("change", createLayerList);
        select.addEventListener("change", removeAlert);
    }
};

function groupCap(select) {
    group = select.value;
};

function download(file) {
    let filename = file.replaceAll(/[A-Z]+\//ig, "");
    element = createAnyElement("a", {
        href: `${file}`,
        download: filename
    });
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
};

function fullDownload() {
    if (typeof group === "undefined" || group.length == 0 || group == 'Válassz egy elemet a listából!') {
        let alertMsg = createAnyElement("div", {
            class: "alert-danger",
            id: "workGroupAlert"
        });
        alertMsg.innerHTML = "Figyelem! Nem választottad ki a munkacsoportot! Nem tudom, melyik mappát kell letölteni.";
        document.getElementById("downloadForm").appendChild(alertMsg);
    }
    else {
        let groupData = new FormData();
        groupData.set("group", group);
        fetchOptions = {
            method: "POST",
            mode: "cors",
            body: groupData
        };
        fetch("lib/browser_app/php/downloadFull.php", fetchOptions).then(
            resp => resp.text()).then(
                downloadLink => download(downloadLink)
            );
    }
};

function createCheckBox(inputID, inputObject) {
    //We will need a "check all/none" checkbox! (https://www.javascripttutorial.net/javascript-dom/javascript-checkbox/)
    let checkBoxForm = document.getElementById(inputID + "Form");
    checkBoxForm.innerHTML = "";
    if (JSON.stringify(inputObject).indexOf("-wal") !== -1) {
        let alertDiv = createAnyElement("div", {
            class: "alert-danger"
        });
        checkBoxForm.appendChild(alertDiv);
        alertDiv.innerHTML = `Figyelem! Az inaktív elemek megnyitott állapotban vannak! Nem ajánlott dolgozni velük! <br>
        Ha mégis szükséged van ezekre az állományokra, vagy a teljes könyvtárral szeretnél dolgozni,
        vedd fel a kapcslatot a tulajdonosával!`
    }
    for (k in inputObject) {
        let inputDiv = createAnyElement("div");
        let inputElement = createAnyElement("input", {
            type: "checkbox",
            value: inputObject[k],
            id: inputID + inputObject[k],
            class: "form-check-input",
            name: inputID + "CheckBoxes"
        });
        if ((inputObject[k]).indexOf("-wal") !== -1) {
            inputElement.setAttribute("disabled", "true");
        }
        inputDiv.appendChild(inputElement);
        let labelElement = createAnyElement("label", {
            for: inputID + inputObject[k],
            class: "form-check-label"
        });
        labelElement.innerHTML = String(inputObject[k])
        inputDiv.appendChild(labelElement);
        checkBoxForm.appendChild(inputDiv);
    }
    let buttonGroup = createAnyElement("div", {
        type: "div",
        class: "btn-group buttonDiv",
        role: "group"
    });
    checkBoxForm.appendChild(buttonGroup);
    var event = `downloadFiles('${inputID}')`;
    let saveButton = createButton(inputID, event, "Kiválasztottak mentése");
    buttonGroup.appendChild(saveButton);
    if (inputID == "dataBackup" ){
        event = "workWithData()";
        let workDataButton = createButton(inputID, event, "Munka az adatokkal");
        buttonGroup.appendChild(workDataButton);
    }
};

function createFileList(checkBoxID, filetype) {
    let groupData = new FormData();
    groupData.set("group", group);
    groupData.set("filetype", filetype);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: groupData
    };

    fetch("lib/browser_app/php/fileLister.php", fetchOptions).then(
        resp => resp.json()
    ).then(
        files => createCheckBox(checkBoxID, files)
    );
};

function createAllCheckBoxes() {
    for (k in listParams) {
        createFileList(k, listParams[k])
    }
};

function downloadFiles(checkBoxName) {
    checkBoxFormId = (checkBoxName + "Form");
    checkBoxName = (checkBoxName + "CheckBoxes");
    let checkBoxes = document.querySelectorAll("input[name=" + checkBoxName + "]:checked");
    let selectedFiles = [];
    checkBoxes.forEach((checkBox) => {
        selectedFiles.push(checkBox.value);
    });

    if (selectedFiles.length == 0) {
        if (!document.getElementById(checkBoxName + "Alert")) {
            let alertMsg = createAnyElement("div", {
                class: "alert-danger",
                id: checkBoxName + "Alert"
            });
            alertMsg.innerHTML = "Figyelem! Nem választottál ki egyetlen fájlt sem! Nem tudom, mivel szeretnél dolgozni.";
            document.getElementById(checkBoxFormId).appendChild(alertMsg);
        }
        let activeCheckBoxes = document.querySelectorAll("input[name=" + checkBoxName + "]");
        for (var i = 0; i < activeCheckBoxes.length; i++) {
            activeCheckBoxes[i].addEventListener("change", removeAlert2);
        }
    } else {

        let filetype = selectedFiles[0].substring(selectedFiles[0].lastIndexOf(".") + 1);

        let groupData = new FormData();
        groupData.set("group", group);
        groupData.set("filetype", filetype);
        groupData.set("filenames", selectedFiles.toString())
        fetchOptions = {
            method: "POST",
            mode: "cors",
            body: groupData
        };
        fetch("lib/browser_app/php/downloadSelected.php", fetchOptions).then(
            resp => resp.text()).then(
                downloadLink => download(downloadLink));
    }
};

function createButton(inputID, onClickEvent, buttonLabel) {
    let inputForm = document.getElementById(inputID + "Form");
    let button = createAnyElement("button", {
        type: "button",
        class: "btn btn-success selectBtn",
        name: inputID + "Btn",
        onClick: onClickEvent
    });
    button.innerHTML = buttonLabel;
    inputForm.appendChild(button);
    return button;
};

function createLayerList() {
    let form = document.getElementById("layerExportForm");
    form.innerHTML = "";
    let groupData = new FormData();
    groupData.set("group", group);
    fetchOptions = {
        method: "POST",
        mode: "cors",
        body: groupData
    };

    fetch("lib/browser_app/php/layerLister.php", fetchOptions).then(
        resp => resp.json()
    ).then(
        gisLayers => createSelectList('layerExport', gisLayers)
    );

    createButton('layerExport', "obmExport", "OBM export");
};

function removeAlert() {
    let alertId = this.id.replace("List", "Alert");
    alertBox = document.getElementById(alertId);
    if (alertBox) {
        alertBox.remove();
    }
};

function removeAlert2() { //Refaktorálás, átgondolás (mondjuk a formig felmenni!!!
    let alertId = this.name + "Alert";
    alertBox = document.getElementById(alertId);
    if (alertBox) {
        alertBox.remove();
    }
};

function workWithData () {//Részben ugyanaz, mint a downloadFiles, tehát itt is kell refaktorálás!
    let checkBoxName = "dataBackupCheckBoxes";
    let checkBoxFormId = "dataBackupForm";
    let checkBoxes = document.querySelectorAll("input[name=" + checkBoxName + "]:checked");
    let selectedFiles = [];
    checkBoxes.forEach((checkBox) => {
        selectedFiles.push(checkBox.value);
    });

    if (selectedFiles.length == 0) {
        if (!document.getElementById(checkBoxName + "Alert")) {
            let alertMsg = createAnyElement("div", {
                class: "alert-danger",
                id: checkBoxName + "Alert"
            });
            alertMsg.innerHTML = "Figyelem! Nem választottál ki egyetlen fájlt sem! Nem tudom, mivel szeretnél dolgozni.";
            document.getElementById(checkBoxFormId).appendChild(alertMsg);
        }
        let activeCheckBoxes = document.querySelectorAll("input[name=" + checkBoxName + "]");
        for (var i = 0; i < activeCheckBoxes.length; i++) {
            activeCheckBoxes[i].addEventListener("change", removeAlert2);
        }
    } else {
        window.location = `lib/browser_app/html/data.html#group/text='${group}'&files/text='${selectedFiles}'`;
    }
};

let groupPHP = "lib/browser_app/php/groupLister.php";
let workGroups = getPHPJSON(groupPHP).then(
    workGroups => createSelectList('workGroup', workGroups)
);