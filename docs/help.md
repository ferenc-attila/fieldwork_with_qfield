# Súgó

## Munkakönyvtár meghatározása

A tájegység, vagy munkacsoport megadása szükséges ahhoz, hogy a megfelelő állományokkal tudj dolgozni.

## A munkacsoport összes állománynak letöltése

A teljes állomány nagy, akár több gigabájt is lehet, ezért érdemes meggondolni.
Ha azonban módosítani akarod a fájlokat, **mindenképpen érdemes ezzel kezdeni!**

Ha csak az adatokra, vagy a projektfájlokra van szükséged, válaszd azok letöltését (ld. lentebb).

## Terepi adatok letöltése

## Projektfájlok mentése

**Figyelem!** A projektfájl önmagában, azaz a projekt részét alkotó adatbázisok, fedvények, egyéb fájlok nélkül használhatatlan. Ennek a szekciónak a célja, hogy módosítás előtt egy biztonsági mentést tudj magadnak készíteni, vagy frissítsd a helyi használatra szánt projektedet.

## Adatok exportálása

## Projekt generátor